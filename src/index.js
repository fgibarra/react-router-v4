import { applyMiddleware, compose, createStore  } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter} from 'connected-react-router';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'
import rootReducer from './reducers'
import thunk from 'redux-thunk';


const history = createBrowserHistory();



const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    connectRouter(history)(rootReducer),
    composeEnhancer(
        applyMiddleware(
            routerMiddleware(history),
            thunk
        ),
    ),
);

/*
UserIsLogged = (nextState, replace) =>{
    if(store.getState().login.isLogged)
    {
        replace({
            pathname: '/comments'
        })
    }
};

UserNotIsLogged = (nextState, replace) =>{
    if(!store.getState().login.isLogged)
    {
        replace({
            pathname: '/login'
        })
    }
};*/


const render =  () =>{
    ReactDOM.render(
        <Provider store={store}>
            <App history={history}></App>
        </Provider>,
        document.getElementById('root')
    )
};

render();
