import * as types from './actionTypes';
import * as firebase from 'firebase';


const firebaseConfig = {
    apiKey: "AIzaSyAReVkmt-vyHDZxDpe6ZWh_DW9yFfsoXiU",
    authDomain: "practica-react.firebaseapp.com",
    databaseURL: "https://practica-react.firebaseio.com",
    projectId: "practica-react",
    storageBucket: "",
    messagingSenderId: "1039934156871"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export function setUser(user)
{
   return  {
       type: types.LOGIN_USER,
       user
   }
}

export function badUser(data) {
    return {
        type: types.BAD_USER,
        data
    }
}

export function logoutUser()
{
    return {
        type: types.LOGOUT
    }
}



export function  login(data)
{
    return dispatch => {
        firebaseApp.auth().signInWithEmailAndPassword(data.email, data.password)
            .then(function(result){
                sessionStorage.setItem('user', result.uid);
                dispatch(setUser({
                    _id: result.uid,
                    errors: ''
                }));
            }).catch(function(error){
                dispatch(badUser({
                    _id: '',
                    errors: error
                }));
                console.log(error);
        });
    }
}

export function logout(){
    return dispatch => {
        firebaseApp.auth.logout().then(function(result){
            dispatch(logoutUser());
        }).catch(function(error){
                console.log(error);
        });
    };
}
