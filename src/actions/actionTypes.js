/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
//Acciones para el counter
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';

//Acciones para TODO
export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';


//Comentarios
export const FETCH_COMMENTS = 'FETCH_COMMENTS';

//Login
export const LOGIN_USER = 'LOGIN_USER';
export const BAD_USER = 'BAD_USER';
export const LOGOUT = 'LOG_OUT';