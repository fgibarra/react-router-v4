import * as types from './actionTypes';

export function setComments(comments)
{
    return {
        type: types.FETCH_COMMENTS,
        comments
    }
}

export function fetchComments()
{
    return dispatch => {
        fetch('https://jsonplaceholder.typicode.com/comments')
            .then(res => res.json())
            .then(data => {
                dispatch(setComments(data))
            })
    }
}