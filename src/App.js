import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter} from 'connected-react-router';
import routes from "./routes/index";



class App extends Component{

    static propTypes = {
        history: PropTypes.object
    };

    render(){
        const {history} = this.props;

        return(
            <ConnectedRouter history={history}>
                {routes}
            </ConnectedRouter>
        );
    }
}

export default App;
