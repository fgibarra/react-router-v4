/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {increment, decrement} from '../actions/counter';
import {Button, FormControl} from 'react-bootstrap';
import PropTypes from 'prop-types';

class CounterComponent extends Component{
    static propType = {
        counter: PropTypes.number.isRequired,
        increment: PropTypes.func.isRequired,
        decrement: PropTypes.func.isRequired
    };

    render(){
        const wellStyles = { maxWidth: 400, margin: '0 auto 10px'};
        const {counter, increment, decrement} = this.props;

        return(
            <div>
                <h1 className="text-center text-muted">Contador con React + Redux</h1>
                <div className="well" style={wellStyles}>
                    <Button bsStyle ="primary" bsSize = "large" block  onClick={increment}>Incremento</Button>
                    <br />
                    <FormControl type="text" value={counter} disabled/>
                    <Button bsSize="large" block onClick={decrement}>Decremento</Button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state =>({
    counter: state.counter.count
});

const mapDispatchToProps = (dispatch) => {
  return {
      increment: () => {
          dispatch(increment());
      },
      decrement: () => {
          dispatch(decrement());
      }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent);