/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import React, {Component} from 'react';

class HomeComponent extends Component{
    render(){
        return(
            <div>
                <h1 className="text-center text-muted">Home Component</h1>
            </div>
        );
    }
}

export default HomeComponent;