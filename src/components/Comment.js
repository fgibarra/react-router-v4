import React, {Component} from 'react';
import { Panel ,  Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default class Comment extends Component{
    static  propTypes = {
        onClick: PropTypes.func.isRequired,
        comment: PropTypes.object.isRequired
    };

    render(){
        const {onClick, comment} = this.props;

        return(
            <div>
                <Panel header={comment.name}
                        footer={
                         <Button bsStyle="danger" onClick={onClick}>
                             Mostrar Información
                         </Button>}
                >
                    <pre>{comment.body}</pre>
                </Panel>
            </div>
        )
    }
}