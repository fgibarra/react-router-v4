import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default class Todo extends Component{

    static PropTypes = {
        onClick: PropTypes.func.isRequired,
        onDelete: PropTypes.func.isRequired,
        todo: PropTypes.object.isRequired
    }

    render(){
        const {onClick, onDelete, todo} = this.props;

        return(
                <li onClick={onClick} style={{ textDecoration: todo.completed ? 'line-through': 'none', marginBottom: 5}}>
                    {todo.text} <Button bsStyle="danger" onClick={onDelete}>Eliminar</Button>
                </li>
        );
    }
}