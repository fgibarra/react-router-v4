/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import React, {Component} from 'react';
import {NavItem, Navbar, Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';


export default class Navigation extends Component{
    render(){
        return(
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a>React Redux Router App</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        <LinkContainer to="/login">
                            <NavItem eventKey={5}>Login</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/counter">
                            <NavItem eventKey={1}>Counter</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/">
                            <NavItem eventKey={2}>Home</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/todos">
                            <NavItem eventKey={3}>TODOS</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/comentarios">
                            <NavItem eventKey={4}>Comentarios</NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}