import React, {Component} from 'react';
import {connect} from 'react-redux';
import { fetchComments } from "../actions/comments";
import Comment from './Comment';
import PropTypes from 'prop-types'


class CommentComponent extends Component
{
    static PropTypes = {
        comments: PropTypes.array.isRequired,
        fetchComments: PropTypes.func.isRequired
    };

    componentDidMount(){
        this.props.fetchComments();
    }

    render()
    {
        const {comments} = this.props;

        return(
            <div>
                {comments.map(
                    comment =>
                        <Comment key={comment.id} onClick={() => console.log(comment)}  comment={comment}/>
                )}
            </div>
        );
    }
}

const mapStateProps = state => ({
    comments: state.comments
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchComments : () => {
            dispatch(fetchComments());
        }
    }
};

export default connect(mapStateProps, mapDispatchToProps)(CommentComponent);
