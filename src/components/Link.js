import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default class Link extends Component
{
    static propTypes = {
        onClick: PropTypes.func.isRequired,
        active: PropTypes.bool.isRequired,
        children: PropTypes.node.isRequired
    }

    render(){
        const {onClick, active, children} = this.props;

        if(active){
            return(
                <span style={{marginLeft: 20}}>
                    <Button onClick={onClick} bsSize="lg" bsStyle="success">{children}</Button>
                </span>
            );
        }

        return(
            <span style={{marginLeft: 20}}>
                    <Button onClick={onClick} bsSize="lg" bsStyle="danger">{children}</Button>
            </span>
        );
    }
}