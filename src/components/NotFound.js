/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import React from 'react'

class NotFound extends React.Component{
    render(){
        return(
            <div className="well">
                <h2 className="text-center">La ruta no ha sido encontrada</h2>
            </div>
        );
    }
}

export default NotFound;