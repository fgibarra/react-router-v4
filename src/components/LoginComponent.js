import React, {Component} from 'react';
import {Form, FormControl, Checkbox, FormGroup, Col, ControlLabel, Button} from 'react-bootstrap';
import {connect} from 'react-redux';
import {login} from '../actions/login';
import PropTypes from 'prop-types';


class LoginComponent extends Component{
    static propTypes = {
        login: PropTypes.func.isRequired,
        isLogged: PropTypes.bool.isRequired
    };

    static contextTypes = {
        router: PropTypes.object
    };

    constructor(){
        super();

        this.state = {
            email: '',
            password: ''
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.isLogged){
            this.context.router.history.push('/comentarios');
        }
    }

    onSubmit(e){
        e.preventDefault();
        this.props.login(this.state);
    }

    render()
    {
        const {errors} = this.props;
        return(
          <div className="col-md-6 col-md-offset3">
              <h2 className="text-center text-muted">Login ReactJS && Redux</h2>
              <hr/>
              {errors && <div className="alert alert-danger text-center">Datos incorrectos</div>}

              <Form onSubmit={this.onSubmit}>
                  <FormGroup controlId="formHorizontalEmail">
                      <Col componentClass={ControlLabel} sm={2}>
                          Email
                      </Col>
                      <Col sm={10}>
                          <FormControl name="email" type="email" placeholder="Email"
    onChange={(e) => this.setState({[e.target.name]: e.target.value})}/>
                      </Col>
                  </FormGroup>

                  <FormGroup controlId="formHorizontalPassword">
                      <Col componentClass={ControlLabel} sm={2}>
                          Password
                      </Col>
                      <Col sm={10}>
                          <FormControl name="password" type="password" placeholder="Password"
    onChange={(e) => this.setState({[e.target.name]: e.target.value})}/>
                      </Col>
                  </FormGroup>

                  <FormGroup>
                      <Col smOffset={2} sm={10}>
                          <Checkbox>Remember me</Checkbox>
                      </Col>
                  </FormGroup>

                  <FormGroup>
                      <Col smOffset={2} sm={10}>
                          <Button type="submit" block bsStyle="primary">
                              Iniciar sesión
                          </Button>
                      </Col>
                  </FormGroup>
              </Form>

          </div>
        );

    }
}

const mapStateToProps = state =>({
    errors: state.login.errors,
    isLogged: state.login.isLogged
});

const mapDispatchToProps = (dispatch) =>{
    return {
        login: (user) =>{
            dispatch(login(user));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent);