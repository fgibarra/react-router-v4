/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import React from 'react'
import { Route, Switch } from 'react-router';
import Home from '../components/HomeComponent';
import CounterComponent from '../components/CounterComponent';
import NavigationComponent from '../components/NavigationComponent';
import NotFound from '../components/NotFound';
import TodoListComponent from '../components/TodoListComponent';
import CommentComponent from '../components/CommentComponent';
import LoginComponent from '../components/LoginComponent';

const routes = (
    <div>
        <NavigationComponent/>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={LoginComponent}/>
            <Route path="/counter" component={CounterComponent} />
            <Route path="/todos" component={TodoListComponent} />
            <Route path="/comentarios" component={CommentComponent} />
            <Route component={NotFound}/>
        </Switch>
    </div>
);




/*const routes = (isLogged, isNotLogged) => {
    return (
        <div>
            <NavigationComponent/>
            <Switch>
                <Route exact path="/" component={Home} onEnter={isNotLogged}/>
                <Route path="/login" component={LoginComponent} onEnter={isLogged}/>
                <Route path="/counter" component={CounterComponent} onEnter = {isNotLogged}/>
                <Route path="/todos" component={TodoListComponent} onEnter = {isNotLogged}/>
                <Route path="/comentarios" component={CommentComponent} onEnter = {isNotLogged}/>
                <Route component={NotFound}/>
            </Switch>
        </div>
    );
};*/


export default routes

