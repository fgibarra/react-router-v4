/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import * as types from '../actions/actionTypes';

const initialState = {
  count: 0
};

export default function reducer(state = initialState, action = {}) {
    switch (action.type){
        case types.INCREMENT:
            return {
                count: state.count + 1
            }
        case types.DECREMENT:
            return {
                count: state.count - 1
            }
        default:
            return state;
    }
}