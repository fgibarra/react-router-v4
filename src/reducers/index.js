/**
 * Created by Francisco Ibarra on 26/07/2017.
 */
import { combineReducers } from 'redux'
import counter from './counter'
import todos from './todos';
import visibilityFilter from './visibilityFilter';
import comments from './comments';
import login from './login';


const rootReducer = combineReducers({
    counter,
    todos,
    comments,
    login,
    visibilityFilter
});



export default rootReducer;